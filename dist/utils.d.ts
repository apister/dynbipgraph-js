export function randomIntFromInterval(min: any, max: any): number;
export function intersect(a: any, b: any): any[];
export function mergeMaps(s: any, t: any): Map<any, any>;
export function flatten2dArray(array: any): any[];
export function allPairs(array: any): any;
export function groupBy(xs: any, key: any): any;
export function isObject(yourVariable: any): boolean;
